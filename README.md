# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Learning C++ and ECS achitechture

### How do I get set up? ###

* Download the reposetory 
* Project should be already configured with VS 2019

### TODO ###

* Support Multipler Players locally 
* Restracture HUD System to be component based Display text component that is attached on the player entity
* Change lifes presentation from Text to Textures
* Improve preformacne by switching to Owner Group interators in ENTT
* Remove Brick padding and make it more visual
* Fix input system on game over restart state
* Write Audio Engine
* Remove or simplify use of variant unclear at Run-time which info we are trying to access in CCollisionComponent
* engine::MAppManager::SetAppState Remove registry dependency 
* MSystemsManager::~MSystemsManager() automate System destructor
* Make All UI Scale with Screen