#include "PCH.h"
#include "game/Systems/SPlayerSystem.h"

namespace
{
	inline constexpr float s_PlayerHeightMultiplayer = 0.02f;
	inline constexpr float s_PlayerWidthMultiplayer = 0.2f;
}

game::SPlayerSystem::SPlayerSystem(engine::MAppManager& appManager)
	:m_AppManager(appManager)
{

}

void game::SPlayerSystem::Activate(entt::registry& registry)
{
	CreatePlayer(registry);
}

void game::SPlayerSystem::Update(entt::registry& registry)
{
	if (m_AppManager.GetCurrentAppState(registry) == AppState::AppState_Resetting)
	{
		ResetAllPlayers(registry);
	}
}

void game::SPlayerSystem::CreatePlayer(entt::registry& registry, sf::Color color /*sf::Color::Transparent*/)
{
	auto playerEntity = registry.create();
	// check if the Color is chosen by the player otherwise Auto assign it based on the number of players
	if (color == sf::Color::Transparent)
	{
		color = GetNewPlayerAutomaticColor(registry);
	}
	CreatePlayerShape(registry, playerEntity, color);
	CreatePlayerTransform(registry, playerEntity);
	CreatePlayerInputControls(registry, playerEntity);

	//Adds a Player Paddle component
	auto playerPaddleView = registry.view<CPlayerPaddleComponent>();
	int numberOfPlayer = static_cast<int>(playerPaddleView.size());
	numberOfPlayer++;
	registry.emplace<CPlayerPaddleComponent>(playerEntity, color, numberOfPlayer);

	//Create Physics
	//Player considered Input controller as it is not govern by game physics
	registry.emplace<CColliderComponent>(playerEntity, CColliderComponent::ColliderType::ColliderType_InputControlled);
}

void game::SPlayerSystem::CreatePlayerShape(entt::registry& registry, entt::entity& playerEntity, const sf::Color& color)
{
	auto rectangle = std::unique_ptr<sf::RectangleShape>{ std::make_unique<sf::RectangleShape>(sf::RectangleShape()) };
	rectangle->setFillColor(color);
	// Make it a rectangle
	registry.emplace<CRectangleShapeComponent>(playerEntity, *rectangle);
}

void game::SPlayerSystem::CreatePlayerTransform(entt::registry& registry, entt::entity& playerEntity)
{
	//Get the application window 
	const sf::RenderWindow& window = m_AppManager.GetAppWindow();

	//Calculate Position and Size based on window size
	sf::Vector2f position;
	sf::Vector2f velocity(0.f, 0.f);
	sf::Vector2f size;
	CalculatePlayerSize(window, position, size);

	// Let it have a shape, transform, scale, and rotation
	registry.emplace<CTransformComponent>(playerEntity, position, velocity, size);
}

void game::SPlayerSystem::CreatePlayerInputControls(entt::registry& registry, entt::entity& entity)
{
	//Adds input compoent to be able to control the Ractangle with keyboard and mouse
	std::vector<sf::Keyboard::Key> keyboardControls = { sf::Keyboard::Right, sf::Keyboard::Left };
	std::vector<sf::Mouse::Button> mouseControls = { sf::Mouse::Button::Left };
	std::vector<sf::Mouse::Movement> mouseMovement = { sf::Mouse::Movement::Horizontal };
	registry.emplace<CInputComponent>(entity, keyboardControls, mouseControls, mouseMovement);
}

void game::SPlayerSystem::CalculatePlayerSize(const sf::RenderWindow& window, sf::Vector2f& outPosition, sf::Vector2f& outSize)
{
	//Get Window Size and precalucalted Height
	int windowWidth = window.getSize().x;
	int windowHeight = window.getSize().y;

	float playerHeight = windowHeight * s_PlayerHeightMultiplayer;
	float playerWidth = windowWidth * s_PlayerWidthMultiplayer;

	float playerXPosition = windowWidth * s_Half;
	float playerYPosition = windowHeight - (playerHeight * s_Double);

	outPosition = { playerXPosition, playerYPosition };
	outSize = { playerWidth, playerHeight };
}

const sf::Color& game::SPlayerSystem::GetNewPlayerAutomaticColor(entt::registry& registry)
{
	auto playerPaddleView = registry.view<CPlayerPaddleComponent>();
	size_t numberOfPlayer = playerPaddleView.size();
	//Currently Max to 4 players supported

	ASSERT(numberOfPlayer <= 3 && "Unsupported number of players");

	switch (numberOfPlayer)
	{
	case 1:
		return sf::Color::Blue;
		break;
	case 2:
		return sf::Color::Yellow;
		break;
	case 4:
		return sf::Color::Magenta;
		break;
	default:
		return sf::Color::Green;
		break;
	}
}

void game::SPlayerSystem::ResetAllPlayers(entt::registry& registry)
{
	auto playerPaddleView = registry.view<CPlayerPaddleComponent>();
	for (auto& entity : playerPaddleView)
	{
		if (registry.valid(entity))
		{
			CPlayerPaddleComponent& player = registry.get<CPlayerPaddleComponent>(entity);
			player.m_Lifes = 3;
			player.m_Score = 0;
		}
	}
}