#include "PCH.h"
#include "Game/Systems/SGameInputSystem.h"

namespace
{
	// s_KeyboardInputVelocity is used to add the paddle velocity to the ball on release
	inline constexpr int s_KeyboardInputVelocity = 100;
	inline constexpr int s_KeyboardInputVelocityMutiplayer = 20;
	inline constexpr int s_MouseInputVelocityMultiplier = 30;
}

game::SInputSystem::SInputSystem(engine::MAppManager& appManager, SPlayerBallSystem& playerBallSystem)
	:m_AppManager(appManager),
	m_PlayerBallSystem(playerBallSystem)
{

}
void game::SInputSystem::Update(entt::registry& registry)
{
	ListenToPlayerPaddleInput(registry);
	ListenToPlayerBallInput(registry);
}


void game::SInputSystem::ListenToPlayerPaddleInput(entt::registry& registry)
{
	//Early out if we are not running yet
	if (m_AppManager.GetCurrentAppState(registry) != AppState::AppState_Running)
		return;

	const sf::RenderWindow& window = m_AppManager.GetAppWindow();
	// Grab all player input controls
	auto playerPaddleInputView = registry.view<CPlayerPaddleComponent, CTransformComponent, CInputComponent>();

	for (auto& entity : playerPaddleInputView)
	{
		CTransformComponent& transformComponent = registry.get<CTransformComponent>(entity);
		CInputComponent& inputComponent = registry.get<CInputComponent>(entity);

		// If the Player paddle is controlled by the keyboard stop we set the velocity while the key is pressed
		if (inputComponent.m_CurrentControlScheme == CInputComponent::ControlScheme::ControlScheme_Keyboard)
		{
			transformComponent.m_Velocity = { 0,0 };
		}
		// There's checks every key or mouse button that is registered for that entity.

		for (auto& currentKey : inputComponent.m_KeyboardControls)
		{
			if (sf::Keyboard::isKeyPressed(currentKey))
			{
				//Change control scheme on first keybaord input. Used to block mouse based movement while controling with the mouse
				if (inputComponent.m_CurrentControlScheme == CInputComponent::ControlScheme::ControlScheme_Mouse)
				{
					inputComponent.m_CurrentControlScheme = CInputComponent::ControlScheme::ControlScheme_Keyboard;
				}

				switch (currentKey)
				{
				case sf::Keyboard::Right:
					transformComponent.m_Velocity.x = s_KeyboardInputVelocity * s_KeyboardInputVelocityMutiplayer;
					break;
				case sf::Keyboard::Left:
					transformComponent.m_Velocity.x = -s_KeyboardInputVelocity * s_KeyboardInputVelocityMutiplayer;
					break;
				default:
					break;
				}
			}
		}

		for (auto& mouseButton : inputComponent.m_MouseButtons)
		{
			//Listen to any mouse button input to switch control schemes
			if (sf::Mouse::isButtonPressed(mouseButton))
			{
				if (inputComponent.m_CurrentControlScheme == CInputComponent::ControlScheme::ControlScheme_Keyboard)
				{
					inputComponent.m_CurrentControlScheme = CInputComponent::ControlScheme::ControlScheme_Mouse;
				}
			}
		}
		for (auto& mouseMovement : inputComponent.m_MouseMovement)
		{
			//Only enable mouse movement if the current control scheme is mouse
			if (inputComponent.m_CurrentControlScheme == CInputComponent::ControlScheme::ControlScheme_Mouse)
			{
				if (mouseMovement == sf::Mouse::Movement::Horizontal)
				{
					// Map mouse coordinates to window coordinates
					const sf::Vector2i position = sf::Mouse::getPosition(window);
					sf::Vector2f position_f = window.mapPixelToCoords(sf::Mouse::getPosition(window));

					//Velocity is detrined between the position delta of the mouse and previous frame's transform
					transformComponent.m_Velocity.x = (position.x - transformComponent.m_Position.x) * s_MouseInputVelocityMultiplier;
				}
			}
		}
	}
}

void game::SInputSystem::ListenToPlayerBallInput(entt::registry& registry)
{
	//Early out if we are not running yet
	if (m_AppManager.GetCurrentAppState(registry) != AppState::AppState_Running)
		return;
	registry.view<CInputComponent, CPlayerBallComponent>().each([&](auto entity, CInputComponent& inputComponent, CPlayerBallComponent& playerBall)
	{
		if (playerBall.m_Docked)
		{
			for (auto& mouseButton : inputComponent.m_MouseButtons)
			{
				//Listen to release mouse button input on the ball component
				if (sf::Mouse::isButtonPressed(mouseButton))
				{
					m_PlayerBallSystem.ReleaseBall(registry, entity);
				}
			}

			for (auto& key : inputComponent.m_KeyboardControls)
			{
				//Listen to release mouse button input on the ball component
				if (sf::Keyboard::isKeyPressed(key))
				{
					m_PlayerBallSystem.ReleaseBall(registry, entity);
				}
			}
		}
	});
}