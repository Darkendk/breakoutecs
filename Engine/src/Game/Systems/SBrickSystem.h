#include <entt/entt.hpp>
#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>

#include "Engine/Components/CShapeComponent.h"
#include "Engine/Components/CTransformComponent.h"
#include "Engine/Components/CColliderComponent.h"
#include "Engine/Components/CCollisionComponent.h"

#include "Engine/Managers/MAppManager.h"
#include "Engine/Managers/MSoundManager.h"

#include "Engine/Systems/System.h"


#include "Game/Components/CBrickComponent.h"
#include "Game/Components/CPlayerBallComponent.h"
#include "Game/Components/CPlayerPaddleComponent.h"

#include "Game/Systems/SPlayerBallSystem.h"
#include "Game/Systems/SComboSystem.h"

namespace game
{
	class SBrickSystem : System
	{
	public:
		SBrickSystem(engine::MAppManager& appManager, game::SPlayerBallSystem& playerBallSystem, game::SComboSystem& comboSystem, engine::MSoundManager& soundManager);
		void Activate(entt::registry& registry);
		void Update(entt::registry& registry);
		void CreateBrick(entt::registry& registry ,const sf::Vector2f& size, const sf::Vector2f& position);
		void CalculateBrickSizeAndInitalPosition(sf::Vector2f& outPosition, sf::Vector2f& outSize);
		void OnBrickCollision(entt::registry& registry, const entt::entity& sourceEntitiy, const entt::entity& brick);
		void ResetBricks(entt::registry& registry);
	private:
		engine::MAppManager& m_AppManager;
		engine::MSoundManager& m_SoundManager;
		game::SPlayerBallSystem& m_PlayerBallSystem;
		game::SComboSystem& m_ComboSystem;
	};
}