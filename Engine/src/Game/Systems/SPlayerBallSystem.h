#pragma once
#include <entt/entt.hpp>

#include "Engine/Components/CTransformComponent.h"
#include "Engine/Components/CInputComponent.h"
#include "Engine/Components/CShapeComponent.h"
#include "Engine/Components/CColliderComponent.h"
#include "Engine/Components/CCollisionComponent.h"

#include "Engine/Systems/System.h"

#include "Engine/Managers/MSoundManager.h"

#include "Game/Components/CPlayerBallComponent.h"

#include "Game/Systems/SGameplayHUDSystem.h"
#include "Game/Systems/SPlayerSystem.h"

/// \brief Render System in charge or drawing all shapes and objects on the screen
namespace game
{
	class SPlayerBallSystem : System
	{
	public:

		SPlayerBallSystem(engine::MAppManager& appManager, engine::MSoundManager& soundManager);
		void Activate(entt::registry& registry);
		void CreateBall(entt::registry& registry, const entt::entity& playerEntity);

		void Update(entt::registry& registry);
		void UpdateBallPosition(entt::registry& registry);
		void ReleaseBall(entt::registry& registry, const entt::entity& ballEntity);
		void HandleBallCollisions(entt::registry& registry);
		void RedockBall(entt::registry& registry, const entt::entity& ballEnttiy, bool loselife);
		void OnBrickCollision(entt::registry& registry, const entt::entity& ballEnttiy);
		void ResetAllBalls(entt::registry& registry);
	private:
		sf::Vector2f CalculateInitDockingPosition(const CTransformComponent& playerPaddleTransform, const CTransformComponent& ballTransform);
		engine::MAppManager& m_AppManager;
		engine::MSoundManager& m_SoundManager;

	};
}
