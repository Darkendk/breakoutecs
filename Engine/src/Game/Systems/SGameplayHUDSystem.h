#pragma once
#include <map>

#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>

#include "Engine/Managers/MAppManager.h"
#include "Engine/Systems/System.h"

#include "Game/Components/CPlayerPaddleComponent.h"
#include "Game/Components/CPlayerComboMessage.h"
namespace game
{
	class SGameplayHUDSystem : System
	{
	public:

		SGameplayHUDSystem(engine::MAppManager& appManager);
		void Activate(entt::registry& registry);
		void Update(entt::registry& registry, float deltaTime);
		void RequestPlayerCombo(const int& comboCount, entt::entity entity);

	private:
		void DrawPlayerStats(entt::registry& registry, const CPlayerPaddleComponent& playerPuddle, sf::RenderWindow& window, sf::Text& text, const std::string& scoreToShow);
		void DrawGameResult(entt::registry& registry, sf::RenderWindow& window, sf::Text& text, const std::string& scoreToShow);
		void HandleComboRequests(entt::registry& registry);
		void ShowComboMessages(entt::registry& registry, sf::RenderWindow& window, float deltaTime, sf::Text& text);
		const char* GetComboFlavourText(const int& comboCount);
		engine::MAppManager& m_AppManager;
		sf::Font m_Font;

		std::map<entt::entity, int> m_PlayerComboComponentRequests;
	};
}
