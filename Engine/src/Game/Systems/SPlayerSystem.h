#pragma once
#include "Engine/Systems/System.h"

#include "Engine/Managers/MAppManager.h"

#include "Engine/Components/CTransformComponent.h"
#include "Engine/Components/CShapeComponent.h"
#include "Engine/Components/CInputComponent.h"
#include "Engine/Components/CColliderComponent.h"

#include "Game/Components/CPlayerPaddleComponent.h"

namespace game
{
	class SPlayerSystem : System
	{
	public:
		SPlayerSystem(engine::MAppManager& appManager);
		void Activate(entt::registry& registry);
		void Update(entt::registry& registry);

		/// \brief Create a player with all the components that comprize him needed.
		/// \param registry to create and register components
		/// \param entity Player Entity
		void CreatePlayer(entt::registry& registry, sf::Color color = sf::Color::Transparent);

		/// \brief Create a player with all the components that comprize him needed.
		/// \param registry to create and register components
		/// \param entity Player Entity
		void CreatePlayerShape(entt::registry& registry, entt::entity& entity, const sf::Color& color);

		/// \brief Create a player's transform based on the window size
		/// \param registry to create and register components
		/// \param entity Player Entity
		void CreatePlayerTransform(entt::registry& registry, entt::entity& entity);

		/// \brief Create a player's Input controls
		/// \param registry to create and register components
		/// \param entity Player Entity
		void CreatePlayerInputControls(entt::registry& registry, entt::entity& entity);

		/// \brief Calculates the player size based on the currnent winodow's dimentions
		/// \param window the current winow to base the palyer's size based on the current resolution
		/// \param outPosition returns the position of the player to be in the screen center after a resize
		/// \param outSize returns size of the player based on the current open window
		void CalculatePlayerSize(const sf::RenderWindow& window, sf::Vector2f& outPosition, sf::Vector2f& outSize);

		/// \brief Resets the score and life of all players
		void ResetAllPlayers(entt::registry& registry);

		/// \brief Return a predefined Color based on the nubmer of players
		const sf::Color& GetNewPlayerAutomaticColor(entt::registry& registry);
	private:
		engine::MAppManager& m_AppManager;
	};
}