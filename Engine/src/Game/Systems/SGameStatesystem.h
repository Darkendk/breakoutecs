#pragma once
#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>

#include "Engine/Systems/System.h"

#include "Engine/Managers/MAppManager.h"
#include "Engine/Managers/MSoundManager.h"

namespace game
{
	class SGameStateSystem : System
	{
	public:
		SGameStateSystem(engine::MAppManager& appManager, engine::MSoundManager& soundManager);
		void Update(entt::registry& registry);

	private:
		engine::MAppManager& m_AppManager;
		engine::MSoundManager& m_SoundManager;

	};
}
