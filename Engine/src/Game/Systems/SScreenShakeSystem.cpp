#include "PCH.h"
#include "Game/Systems/SScreenShakeSystem.h"

namespace
{
	inline constexpr float s_DefaultCollisionShakeTime = 0.10f;
	inline constexpr int s_LowShakeCount = 2;
	inline constexpr int s_LowComboShakeValue = 4;
	inline constexpr int s_MidShakeCount = 10;
	inline constexpr int s_MidComboShakeValue = 6;
	inline constexpr int s_HighShakeCount = 20;
	inline constexpr int s_HighComboShakeValue = 10;
}
game::SScreenShakeSystem::SScreenShakeSystem(engine::MAppManager& appManager, game::SGameplayHUDSystem& gameplayHUDSystem)
	:m_AppManager(appManager),
	m_GameplayHUDSys(gameplayHUDSystem)
{

}

void game::SScreenShakeSystem::Update(entt::registry& registry, float deltaTime)
{
	registry.view<CCollisionComponent, CPlayerBallComponent>().each([&](auto entity, CCollisionComponent& collision, CPlayerBallComponent& playerBall)
	{
		if (registry.valid(playerBall.m_Player))
		{
			if (collision.m_CollisionInfo.index() == 1)
			{
				const entt::entity& otherEntity = std::get<entt::entity>(collision.m_CollisionInfo);
				if (playerBall.m_Player != otherEntity)
				{
					StartScreenShake(registry, playerBall.m_Player, s_DefaultCollisionShakeTime);
				}
			}
			// in case of window border collision do not shake the screen
		}

	});


	registry.view<CScreenShakeComponent>().each([&](auto entity, CScreenShakeComponent& screenShakeComponent)
	{
		if (screenShakeComponent.m_ShakeTimer > 0)
		{
			screenShakeComponent.m_ShakeTimer -= deltaTime;
			ShakeWindow(screenShakeComponent);
		}
		else
		{
			m_AppManager.GetNoneConstAppWindow().setPosition(screenShakeComponent.m_ScreenInitPosition);
			registry.remove<CScreenShakeComponent>(entity);
		}
		
	});
}

void game::SScreenShakeSystem::StartScreenShake(entt::registry& registry,const entt::entity& player, float shakeTime)
{
	//If we are already shaking we replace the component with a new one
	if (registry.has<CScreenShakeComponent>(player))
	{
		CScreenShakeComponent& shakeComponent = registry.get<CScreenShakeComponent>(player);
		shakeComponent.m_ShakeCounter++ ;
		shakeComponent.m_ShakeTimer = shakeTime;
		//Adjust Shake intesity if needed 
		if (shakeComponent.m_ShakeCounter >= s_HighShakeCount)
		{
			shakeComponent.m_ShakeIntencity = s_HighComboShakeValue;
		}
		else if (shakeComponent.m_ShakeCounter >= s_MidShakeCount)
		{
			shakeComponent.m_ShakeIntencity = s_MidComboShakeValue;
		}
		else if(shakeComponent.m_ShakeCounter >= s_LowShakeCount)
		{
			shakeComponent.m_ShakeIntencity = s_LowComboShakeValue;
		}
	}
	else
	{
		registry.emplace<CScreenShakeComponent>(player, shakeTime, m_AppManager.GetAppWindow().getPosition());
	}
}

void game::SScreenShakeSystem::ShakeWindow(const CScreenShakeComponent& screenShakeComponent)
{
	//Always shake from the initial position
	m_AppManager.GetNoneConstAppWindow().setPosition(screenShakeComponent.m_ScreenInitPosition);

	m_AppManager.GetNoneConstAppWindow().setPosition(sf::Vector2i(
		screenShakeComponent.m_ScreenInitPosition.x + rand() % screenShakeComponent.m_ShakeIntencity,
		screenShakeComponent.m_ScreenInitPosition.y + rand() % screenShakeComponent.m_ShakeIntencity));
}