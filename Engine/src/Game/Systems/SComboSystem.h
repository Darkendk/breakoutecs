#pragma once

#include "engine/Managers/MAppManager.h"

#include "engine/Systems/System.h"

#include "game/Components/CComboComponent.h"
#include "game/Components/CScreenShakeComponent.h"

#include "game/Systems/SGameplayHUDSystem.h"


namespace game
{
	class SComboSystem : System
	{
	public:

		SComboSystem(engine::MAppManager& appManager, SGameplayHUDSystem& gameplayHUDSystem);
		void Update(entt::registry& registry, float deltatime);
		void OnBrickCollision(entt::registry& registry, const entt::entity& sourceEntitiy);

	private:
		void OnComboEnded(entt::registry& registry, const int& comboCount, const entt::entity& sourceEntitiy);
		void IncreaseCombo(entt::registry& registry, const entt::entity& playerEnttiy);
		engine::MAppManager& m_AppManager;
		game::SGameplayHUDSystem& m_gameplayHUDSystem;
	};
}
