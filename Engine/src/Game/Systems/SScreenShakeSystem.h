#pragma once
#include <entt/entt.hpp>
#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>

#include "Engine/Components/CCollisionComponent.h"
#include "Engine/Components/CShapeComponent.h"

#include "Engine/Managers/MAppManager.h"

#include "Engine/Systems/System.h"

#include "Game/Components/CScreenShakeComponent.h"
#include "Game/Components/CPlayerBallComponent.h"

#include "Game/Systems/SGameplayHUDSystem.h"

namespace game
{
	class SScreenShakeSystem : System
	{
	public:
		SScreenShakeSystem(engine::MAppManager& appManager, game::SGameplayHUDSystem& gameplayHUDSystem);
		void Update(entt::registry& registry, float deltaTime);
		void StartScreenShake(entt::registry& registry, const entt::entity& player, float shakeTime);
		void ShakeWindow(const CScreenShakeComponent& screenShakeComponent);
	private:
		engine::MAppManager& m_AppManager;
		game::SGameplayHUDSystem& m_GameplayHUDSys;
	};
}