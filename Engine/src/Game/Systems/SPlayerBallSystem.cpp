#include "PCH.h"
#include "SPlayerBallSystem.h"

namespace
{
	// at 1920 * 0.015 = ~28 radius 
	inline constexpr float  s_PlayerBallRadiusMultiplayer = 0.015f;
	inline constexpr int	s_PlayerBallPointCount = 30;
	inline constexpr float	s_BallInitialVelocity = -1000.0f;
	inline constexpr float	s_BallMaxVelocity = 1300.0f;
}

game::SPlayerBallSystem::SPlayerBallSystem(engine::MAppManager& appManager, engine::MSoundManager& soundManager)
	:m_AppManager(appManager)
	,m_SoundManager(soundManager)
{

}

void game::SPlayerBallSystem::Activate(entt::registry& registry)
{
	auto playerPaddleView = registry.view<CPlayerPaddleComponent>();
	for (auto& player : playerPaddleView)
	{
		if (registry.valid(player))
		{
			CreateBall(registry, player);
		}
	}
}

void game::SPlayerBallSystem::CreateBall(entt::registry& registry, const entt::entity& player)
{
	//Create a Entity 
	auto ballEntity = registry.create();

	//Create Ball Shape
	auto circle = std::unique_ptr<sf::CircleShape>{std::make_unique<sf::CircleShape>(sf::CircleShape())};
	//Color 
	sf::Color color = sf::Color::Blue;
	if (registry.has<CPlayerPaddleComponent>(player))
	{
		const CPlayerPaddleComponent& playerPaddle = registry.get<CPlayerPaddleComponent>(player);
		//Adjust Ball Color based ont the player Color
		color = playerPaddle.m_Color;
		color.r += std::clamp(color.r + 128, 1, 255);
		color.g += std::clamp(color.g + 128, 1, 255);
		color.b += std::clamp(color.b + 128, 1, 255);
	}
 	circle->setFillColor(color);
	registry.emplace<CCircleShapeComponent>(ballEntity, *circle);

	// Create Ball Tansform
	sf::Vector2f position(0.f, 0.f);
	sf::Vector2f velocity(0.f, 0.f);
	
	int windowWidth = m_AppManager.GetAppWindow().getSize().x;
	float ballRadius = windowWidth * s_PlayerBallRadiusMultiplayer;
	//Storing the full size of the ball for all calucations
	sf::Vector2f size(ballRadius*s_Double, s_PlayerBallPointCount);
	CTransformComponent& ballTransform = registry.emplace<CTransformComponent>(ballEntity, position, velocity, size);

	//Create Ball Input
	std::vector<sf::Keyboard::Key> keyboardControls = { sf::Keyboard::Space };
	std::vector<sf::Mouse::Button> mouseControls = { sf::Mouse::Button::Left };
	registry.emplace<CInputComponent>(ballEntity, keyboardControls, mouseControls);

	//Assigns the player to the Ball
	if (registry.has<CTransformComponent>(player))
	{
		const CTransformComponent& playerPaddlePosition = registry.get<CTransformComponent>(player);
		//Adjust init position to be the at the center of the player's Paddle
		position = CalculateInitDockingPosition(playerPaddlePosition, ballTransform);
	}

	registry.emplace<CColliderComponent>(ballEntity, CColliderComponent::ColliderType::ColliderType_Dynaimc);
	registry.emplace<CPlayerBallComponent>(ballEntity, player, true, position);
}


void game::SPlayerBallSystem::Update(entt::registry& registry)
{
	UpdateBallPosition(registry);
	HandleBallCollisions(registry);
	if (m_AppManager.GetCurrentAppState(registry) == AppState::AppState_Resetting)
	{
		ResetAllBalls(registry);
	}
}

void game::SPlayerBallSystem::UpdateBallPosition(entt::registry& registry)
{
	registry.view<CTransformComponent, CPlayerBallComponent>().each([&](auto Entity, CTransformComponent& transformComponent, CPlayerBallComponent& playerBall)
	{
		if (playerBall.m_Docked &&
			registry.valid(playerBall.m_Player) &&
			registry.has<CTransformComponent>(playerBall.m_Player))
		{
			//Places the ball at the pladdle
			const CTransformComponent& playerPaddleTransform = registry.get<CTransformComponent>(playerBall.m_Player);
			transformComponent.m_Position = playerPaddleTransform.m_Position - playerBall.m_DockingOffsetPosition;
		}
	});
}

void game::SPlayerBallSystem::ReleaseBall(entt::registry& registry, const entt::entity& ballEnttiy)
{
	if (registry.has<CPlayerBallComponent>(ballEnttiy) && registry.has<CTransformComponent>(ballEnttiy))
	{
		CPlayerBallComponent& ballComponent = registry.get<CPlayerBallComponent>(ballEnttiy);
		CTransformComponent& ballTransfrom = registry.get<CTransformComponent>(ballEnttiy);
		const entt::entity& playerEntity = ballComponent.m_Player;

		//Set Initial Release Velocity
		sf::Vector2f releaseVelocity = { 0.0f, s_BallInitialVelocity };

		//Add player current Velocity
		if (registry.valid(playerEntity) && registry.has<CTransformComponent>(playerEntity))
		{
			CTransformComponent& PlayerTransfrom = registry.get<CTransformComponent>(playerEntity);
			//There is a limit to Max Velocity can be added from the player 
			releaseVelocity += PlayerTransfrom.m_Velocity;
		}

		//Release the Ball 
		ballTransfrom.m_Velocity = releaseVelocity;
		//Limit Horizontal velocity to avoid craziness
		ballTransfrom.m_Velocity.x = std::clamp(ballTransfrom.m_Velocity.x, -s_BallMaxVelocity, s_BallMaxVelocity);
		ballComponent.m_Docked = false;
	}
}

sf::Vector2f game::SPlayerBallSystem::CalculateInitDockingPosition(const CTransformComponent& playerPaddleTransform, const CTransformComponent& ballTransform)
{
	//Ball transform components stores the radius in x member of size
	float ballDiameter = ballTransform.m_Size.x;

	//Calculate off set to place the ball in the center of the player paddle
	sf::Vector2f positionOffSet = { ballDiameter * s_Half - playerPaddleTransform.m_Size.x * s_Half, ballDiameter };
	return positionOffSet;
}

void game::SPlayerBallSystem::HandleBallCollisions(entt::registry& registry)
{
	registry.view<CCollisionComponent,CTransformComponent, CPlayerBallComponent>().each([&](auto entity, CCollisionComponent& collision, CTransformComponent& transformComponent, CPlayerBallComponent& playerBall)
	{
		// 1 is and entity in the Collision Info variant
		if (collision.m_CollisionInfo.index() == 1)
		{
			const entt::entity& otherEntity = std::get<entt::entity>(collision.m_CollisionInfo);
			if (registry.has<CTransformComponent>(otherEntity))
			{
				const CTransformComponent& otherTransform = registry.get<CTransformComponent>(otherEntity);
				sf::Vector2f dir = otherTransform.m_Position - transformComponent.m_Position;
				bool isPlayerPaddle = registry.has<CPlayerPaddleComponent>(otherEntity);

				//Handle Collision
				if (dir.y < 0) // Bottom 
				{
					//If the ball colided with the player paddle from the bottom for any reason it is lost
					if (isPlayerPaddle)
					{
						RedockBall(registry, entity, true);
					}
					else
					{
						transformComponent.m_Velocity.y = -transformComponent.m_Velocity.y;
					}
				}
				else if (dir.y > 0) // Top 
					transformComponent.m_Velocity.y = -transformComponent.m_Velocity.y;
				else if (dir.x < 0) // Left 
					transformComponent.m_Velocity.x = -transformComponent.m_Velocity.x;
				else if (dir.x > 0) // Right 
					transformComponent.m_Velocity.x = -transformComponent.m_Velocity.x;
				
				transformComponent.m_Velocity.x +=  otherTransform.m_Velocity.x;

				//Enforce Velocity limits 
				transformComponent.m_Velocity.x = std::clamp(transformComponent.m_Velocity.x, -s_BallMaxVelocity, s_BallMaxVelocity);

				//Play Sound on Puddle Collision
				if (isPlayerPaddle)
					m_SoundManager.PlaySound("PaddleImpact", false, entity);
			}
		} 
		else if(std::get<CollisionDirection>(collision.m_CollisionInfo) == CollisionDirection::CollisionDirection_Bottom) 
		{// This means a window collision
			RedockBall(registry, entity, true);
		}
		else
		{
			//m_SoundManager.PlaySound("WindowImpact", false, entity);
			switch (std::get<CollisionDirection>(collision.m_CollisionInfo))
			{
			case CollisionDirection::CollisionDirection_Left:
				transformComponent.m_Velocity.x = -transformComponent.m_Velocity.x;
				break;
			case CollisionDirection::CollisionDirection_Right:
				transformComponent.m_Velocity.x = -transformComponent.m_Velocity.x;
				break;
			case CollisionDirection::CollisionDirection_Top:
				transformComponent.m_Velocity.y = -transformComponent.m_Velocity.y;
				break;
			case CollisionDirection::CollisionDirection_Bottom:
				transformComponent.m_Velocity.y = -transformComponent.m_Velocity.y;
				break;
			default:
				break;
			}
		}

	});
}

void game::SPlayerBallSystem::RedockBall(entt::registry& registry, const entt::entity& ballEnttiy, bool loselife)
{
	if (registry.has<CPlayerBallComponent>(ballEnttiy) && registry.has<CTransformComponent>(ballEnttiy))
	{
		CPlayerBallComponent& playerBallComp = registry.get<CPlayerBallComponent>(ballEnttiy);
		// Early out in case the ball is already docked

		//Ball already docked please double check why redocking is called twice
		if (playerBallComp.m_Docked)
			return;

		CTransformComponent& ballTransform = registry.get<CTransformComponent>(ballEnttiy);
		if (registry.valid(playerBallComp.m_Player) && registry.has<CTransformComponent>(playerBallComp.m_Player))
		{
			const CTransformComponent& playerPaddlePosition = registry.get<CTransformComponent>(playerBallComp.m_Player);
			playerBallComp.m_Docked = true;
			ballTransform.m_Velocity = { 0.f,0.f };
			//Place the ball on the paddle platfom based on the precalculated offset 
			ballTransform.m_Position = playerPaddlePosition.m_Position - playerBallComp.m_DockingOffsetPosition;

			//Lower lifes by 1
			if (loselife && registry.has<CPlayerPaddleComponent>(playerBallComp.m_Player))
			{
				CPlayerPaddleComponent& playerPaddleComponent = registry.get<CPlayerPaddleComponent>(playerBallComp.m_Player);
				playerPaddleComponent.m_Lifes--;
				
				// Avoid playing sound on the last BallRedock
				if(playerPaddleComponent.m_Lifes != 0)
					m_SoundManager.PlaySound("BallRedock", false, ballEnttiy);
			}
		}		
	}
}

void game::SPlayerBallSystem::OnBrickCollision(entt::registry& registry, const entt::entity& ballEnttiy)
{
	//Get the Player Puddle and raise the score
	if (registry.has<CPlayerBallComponent>(ballEnttiy))
	{
		const CPlayerBallComponent& playerBallComponent = registry.get<CPlayerBallComponent>(ballEnttiy);

		if (registry.valid(playerBallComponent.m_Player) &&
			registry.has<CPlayerPaddleComponent>(playerBallComponent.m_Player))
		{
			CPlayerPaddleComponent& playerPaddleComponent = registry.get<CPlayerPaddleComponent>(playerBallComponent.m_Player);
			playerPaddleComponent.m_Score++;
		}
	}
}

void game::SPlayerBallSystem::ResetAllBalls(entt::registry& registry)
{
	registry.view<CPlayerBallComponent>().each([&](auto entity, CPlayerBallComponent& playerBall)
	{
		RedockBall(registry, entity, false);
	});
}