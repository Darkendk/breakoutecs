#include "PCH.h"
#include "SGameplayHUDSystem.h"

#include <filesystem>
namespace fs = std::filesystem;

namespace
{
	inline constexpr const char* s_ScoreFontRelativePath = "BlackRocker.ttf"; //Builds automatically copy of the fonts to the root of the .exe
	inline constexpr const char* s_Player = "Player";
	inline constexpr const char* s_Life = "Lifes: ";
	inline constexpr const char* s_ComboFlavour1 = "Nice";
	inline constexpr const char* s_ComboFlavour2 = "Sweet!";
	inline constexpr const char* s_ComboFlavour3 = "OH!";
	inline constexpr const char* s_ComboFlavour4 = "WOW!";
	inline constexpr const char* s_ComboFlavour5 = "WOW!!! AHHI!";
	inline constexpr const char* s_ComboFlavour6 = "!@#$%^&*()!@#$%^&";
	inline constexpr const char* s_GameOver = "Game Over";
	inline constexpr const char* s_Win = "You Won!";
	inline constexpr float s_EndLineSizeMultiplier = 1.15f;
	inline constexpr int S_textSize= 24; // in pixels, not points!
	inline constexpr int S_textSizeGameOverMultiplier = 3; // multiplication for the GameOver text
	inline constexpr float S_DefaultComboMessagelifetime = 1.f; // Combo Massage Lifetime
	inline constexpr int S_MinimumComboCount = 5; // Minimum Combo time
}
game::SGameplayHUDSystem::SGameplayHUDSystem(engine::MAppManager& appManager)
	:m_AppManager(appManager)
{

}

void game::SGameplayHUDSystem::Activate(entt::registry& registry)
{
	//SFML send a message to the log if we failed to load the font
	fs::path fontPath = s_ScoreFontRelativePath;
	bool result = m_Font.loadFromFile(fontPath.string());
	ASSERT(result && "Can't load font")
}

void game::SGameplayHUDSystem::Update(entt::registry& registry, float deltaTime)
{
	sf::RenderWindow& window = m_AppManager.GetNoneConstAppWindow();
	sf::Text text;
	text.setFont(m_Font);
	HandleComboRequests(registry);
	ShowComboMessages(registry, window, deltaTime, text);

	auto playerPaddleView = registry.view<CPlayerPaddleComponent>();
	for (auto& playerEntity : playerPaddleView)
	{
		if (registry.valid(playerEntity))
		{
			//Display Score
			const CPlayerPaddleComponent& playerPuddle = registry.get<CPlayerPaddleComponent>(playerEntity);
			std::string scoreToShow = s_Player + std::to_string(playerPuddle.m_PlayerIndex) + " : " + std::to_string(playerPuddle.m_Score);;

			DrawPlayerStats(registry, playerPuddle, window, text, scoreToShow);

			//Render End Game States 
			DrawGameResult(registry, window, text, scoreToShow);

		}
	}
}

void game::SGameplayHUDSystem::DrawPlayerStats(entt::registry& registry, const CPlayerPaddleComponent& playerPuddle, sf::RenderWindow& window, sf::Text& text, const std::string& scoreToShow)
{
	//Reset Position
	text.setPosition({ 0.f,0.f }); 
	// set the character size
	text.setCharacterSize(S_textSize);

	// set the color
	text.setFillColor(playerPuddle.m_Color);

	//Create the String to display the score
	
	text.setString(scoreToShow);
	window.draw(text);;

	// Display Current Life Count 
	const sf::Vector2f& currentTextPosition = text.getPosition();
	text.setPosition(currentTextPosition.x, currentTextPosition.y + S_textSize * s_EndLineSizeMultiplier);

	std::string lifeToShow = s_Life + std::to_string(playerPuddle.m_Lifes);
	text.setString(lifeToShow);
	window.draw(text);;
}

void game::SGameplayHUDSystem::DrawGameResult(entt::registry& registry, sf::RenderWindow& window, sf::Text& text, const std::string& scoreToShow)
{
	const AppState& appState = m_AppManager.GetCurrentAppState(registry);
	if (appState == AppState::AppState_GameOver ||
		appState == AppState::AppState_Win)
	{
		if (appState == AppState::AppState_GameOver)
		{
			text.setString(s_GameOver);
		}
		else
		{
			text.setString(s_Win);
		}

		text.setCharacterSize(S_textSize * S_textSizeGameOverMultiplier);
		int windowWidth = window.getSize().x;
		int windowHeight = window.getSize().y;
		float gameOverWidth = text.getLocalBounds().width;
		float gameOverXPos = (windowWidth - gameOverWidth) / s_Double;
		text.setPosition(gameOverXPos, (windowHeight - text.getLocalBounds().height) / 2);

		window.draw(text);
		//Playyer Score
		text.setString(scoreToShow);
		text.setCharacterSize(static_cast<int>(S_textSize * S_textSizeGameOverMultiplier * s_Half));
		float scoreWidth = text.getLocalBounds().width;
		text.setPosition((windowWidth - scoreWidth) / s_Double, text.getPosition().y + S_textSize * S_textSizeGameOverMultiplier );
		window.draw(text);
	}
}

void game::SGameplayHUDSystem::RequestPlayerCombo(const int& comboCount, entt::entity entity)
{
	// Don't trigger a combo on every impact
	if (comboCount >= S_MinimumComboCount)
	{
		//in case we get multiple combo requests in the same frame we gather all of them and handle it all in HandleComboRequests
		m_PlayerComboComponentRequests.insert({ entity,comboCount });
	}
}

void game::SGameplayHUDSystem::HandleComboRequests(entt::registry& registry)
{
	for (auto& request: m_PlayerComboComponentRequests)
	{
		entt::entity player = request.first;
		int comboScore = request.second;
		if (registry.has<CPlayerComboMessage>(player))
		{
			CPlayerComboMessage& comboMessage = registry.get<CPlayerComboMessage>(player);
			// If current showing a combo message just update it
			if (comboMessage.m_ComboScore <= comboScore)
			{
				comboMessage.m_ComboScore = comboScore;
				comboMessage.m_Lifetime = S_DefaultComboMessagelifetime;
			}
		}
		else
		{
			registry.emplace<CPlayerComboMessage>(player, comboScore, S_DefaultComboMessagelifetime);
		}
	}
	m_PlayerComboComponentRequests.clear();
}

void game::SGameplayHUDSystem::ShowComboMessages(entt::registry& registry, sf::RenderWindow& window, float deltaTime, sf::Text& text)
{
	registry.view<CPlayerComboMessage, CPlayerPaddleComponent>().each([&](auto entity, CPlayerComboMessage& playerComboMessage, CPlayerPaddleComponent& playerPaddle)
	{
		const int& comboCount = playerComboMessage.m_ComboScore;

		// set the character size
		text.setCharacterSize(S_textSize);

		// set the color
		text.setFillColor(playerPaddle.m_Color);

		//Create the String to display the score
		text.setString(GetComboFlavourText(comboCount));

		int windowWidth = window.getSize().x;
		int windowHeight = window.getSize().y;
		float textWidth = text.getLocalBounds().width;
		float textXPos = (windowWidth - textWidth) / 2;
		text.setPosition(textXPos, (windowHeight - text.getLocalBounds().height) / 2);

		window.draw(text);;

		text.setString(std::to_string(comboCount));
		const sf::Vector2f& currentTextPosition = text.getPosition();
		text.setPosition((windowWidth - text.getLocalBounds().width) / 2, currentTextPosition.y + S_textSize * s_EndLineSizeMultiplier);

		window.draw(text);

		// Handle lifetime 
		if (playerComboMessage.m_Lifetime <= 0)
		{
			registry.remove_if_exists<CPlayerComboMessage>(entity);
		}
		else
		{
			playerComboMessage.m_Lifetime -= deltaTime;
		}

	});
}

const char* game::SGameplayHUDSystem::GetComboFlavourText(const int& comboCount)
{
	if (comboCount >= S_MinimumComboCount * 9)
		return s_ComboFlavour6;
	else if (comboCount >= S_MinimumComboCount * 5)
		return s_ComboFlavour5;
	else if (comboCount >= S_MinimumComboCount * 4)
		return s_ComboFlavour4;
	else if (comboCount >= S_MinimumComboCount * 3)
		return s_ComboFlavour3;
	else if (comboCount >= S_MinimumComboCount* 2)
		return s_ComboFlavour2;

	return s_ComboFlavour1;
}