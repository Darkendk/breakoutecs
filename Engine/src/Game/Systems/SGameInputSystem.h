#pragma once

#include <entt/entt.hpp>
#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>

#include "Engine/Components/CTransformComponent.h"
#include "Engine/Components/CInputComponent.h"

#include "Engine/Managers/MAppManager.h"

#include "Engine/Systems/System.h"

#include "Game/Components/CPlayerPaddleComponent.h"
#include "Game/Components/CPlayerBallComponent.h"

#include "Game/Systems/SPlayerBallSystem.h"

namespace game
{
	class SInputSystem : System
	{
	public:
		SInputSystem(engine::MAppManager& appManager, SPlayerBallSystem& palyerBallSystme);
		void Update(entt::registry& registry);
		void ListenToPlayerPaddleInput(entt::registry& registry);
		void ListenToPlayerBallInput(entt::registry& registry);
	private:
		engine::MAppManager& m_AppManager;
		SPlayerBallSystem& m_PlayerBallSystem;
	};
}