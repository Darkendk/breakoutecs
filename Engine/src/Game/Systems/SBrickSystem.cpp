#include "PCH.h"
#include "Game/Systems/SBrickSystem.h"

namespace
{
	inline constexpr float s_BrickHeightMultiplayer = 0.04f;
	inline constexpr float s_BrickWidthMultiplayer = 0.1f; 
	inline constexpr int	s_BrickFormationCount = 8;
	inline constexpr float s_BrickPadding = 2.0f;
}

game::SBrickSystem::SBrickSystem(engine::MAppManager& appManager, game::SPlayerBallSystem& playerBallSystem, game::SComboSystem& comboSystem, engine::MSoundManager& soundManager)
	:m_AppManager(appManager),
	m_SoundManager(soundManager),
	m_PlayerBallSystem(playerBallSystem),
	m_ComboSystem(comboSystem)
{
}

void game::SBrickSystem::Activate(entt::registry& registry)
{
	//Create a Wall of Bricks
	sf::Vector2f brickSize;
	sf::Vector2f initialBrickPosition;
	CalculateBrickSizeAndInitalPosition(initialBrickPosition, brickSize);
	sf::Vector2f placementPosition  = initialBrickPosition;
	for (size_t i = 0; i < s_BrickFormationCount; i++)
	{
		// place 
		placementPosition.x = initialBrickPosition.x;
		for (size_t j = 0; j < s_BrickFormationCount; j++)
		{
			CreateBrick(registry, placementPosition, brickSize);
			//Create next brick right next to it
			placementPosition.x += (brickSize.x + s_BrickPadding);
		}
		// Move one row down
		placementPosition.y += (brickSize.y + s_BrickPadding);
	}
}

void game::SBrickSystem::Update(entt::registry& registry)
{
	registry.view<CCollisionComponent, CTransformComponent>().each([&](auto entity, CCollisionComponent& collision, CTransformComponent& transformComponent)
	{
		// Ignore Window Border Collision
		if (collision.m_CollisionInfo.index() == 1)
		{
			const entt::entity& otherEntity = std::get<entt::entity>(collision.m_CollisionInfo);
			if (registry.has<CBrickComponent>(otherEntity))
			{
				OnBrickCollision(registry, entity ,otherEntity);
			}
		}
	});
	//Game Over Rest
	if (m_AppManager.GetCurrentAppState(registry) == AppState::AppState_Resetting)
	{
		ResetBricks(registry);
	}
}


void game::SBrickSystem::CreateBrick(entt::registry& registry, const sf::Vector2f& position, const sf::Vector2f& size)
{
	auto brickEntity = registry.create();
	auto rectangle = std::unique_ptr<sf::RectangleShape>{ std::make_unique<sf::RectangleShape>(sf::RectangleShape()) };
	rectangle->setFillColor(sf::Color::Cyan);
	// Make it a rectangle
	registry.emplace<CRectangleShapeComponent>(brickEntity, *rectangle);

	// Adds a transform, scale, and rotation
	registry.emplace<CTransformComponent>(brickEntity, position,sf::Vector2f(0,0), size);

	// Adds a Brick Component
	registry.emplace<CBrickComponent>(brickEntity);

	// Adds Collision to the Bricks
	registry.emplace<CColliderComponent>(brickEntity);
}

void game::SBrickSystem::CalculateBrickSizeAndInitalPosition(sf::Vector2f& outPosition, sf::Vector2f& outSize)
{
	//Get Window Size and precalucalted Height
	const sf::RenderWindow& window = m_AppManager.GetAppWindow();
	int windowWidth = window.getSize().x;
	int windowHeight = window.getSize().y;

	float brickWidth = windowWidth * s_BrickWidthMultiplayer;
	float brickHeight = windowHeight * s_BrickHeightMultiplayer;

	float windowMiddle = windowWidth * s_Half;
	float halfFormationCount = s_BrickFormationCount * s_Half;
	float queterFormationCount = s_BrickFormationCount * s_Queter;

	float brickPositionX = windowMiddle - (halfFormationCount * brickWidth);
	float brickPositionY = brickHeight + (queterFormationCount * brickHeight);

	outPosition = { brickPositionX, brickPositionY };
	outSize = { brickWidth, brickHeight };
}

void game::SBrickSystem::OnBrickCollision(entt::registry& registry, const entt::entity& sourceEntitiy,const entt::entity& brick)
{
	m_PlayerBallSystem.OnBrickCollision(registry, sourceEntitiy);
	m_ComboSystem.OnBrickCollision(registry, sourceEntitiy);
	m_SoundManager.PlaySound("BrickCollision",false,sourceEntitiy);
	registry.destroy(brick);
}

void game::SBrickSystem::ResetBricks(entt::registry& registry)
{
	auto brickComponentView = registry.view<CBrickComponent>();

	for (auto& entity : brickComponentView)
	{
		registry.destroy(entity);
	}
	Activate(registry);
}