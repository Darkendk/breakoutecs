#include "PCH.h"
#include "game/Components/CPlayerBallComponent.h"
#include "game/Systems/SComboSystem.h"

namespace
{
	inline constexpr float s_DefaultComboTimer = 0.3f;
}
game::SComboSystem::SComboSystem(engine::MAppManager& appManager, game::SGameplayHUDSystem& gameplayHUDSystem)
	:m_AppManager(appManager),
	m_gameplayHUDSystem(gameplayHUDSystem)
{

}

void game::SComboSystem::Update(entt::registry& registry, float deltatime)
{
	auto comboComponentView = registry.view<CComboComponent>();

	for (auto& entity : comboComponentView)
	{
		CComboComponent& comboComponent = comboComponentView.get<CComboComponent>(entity);
		if (comboComponent.m_ComboCounter != 0)
		{
			if (comboComponent.m_ComboLifeTime <= 0)
			{
				OnComboEnded(registry, comboComponent.m_ComboCounter,entity);
			}
			//Half way through the combo timer update the HUD
			else if (comboComponent.m_UpdatedHUD == false &&
				comboComponent.m_ComboLifeTime <= s_DefaultComboTimer* s_Half)
			{
				m_gameplayHUDSystem.RequestPlayerCombo(comboComponent.m_ComboCounter, entity);
				comboComponent.m_UpdatedHUD = true;
				comboComponent.m_ComboLifeTime -= deltatime;
			}
			else
			{
				comboComponent.m_ComboLifeTime -= deltatime;
			}
		}
	}
}

void game::SComboSystem::OnBrickCollision(entt::registry& registry, const entt::entity& sourceEntitiy)
{
	if (registry.has<CPlayerPaddleComponent>(sourceEntitiy))
	{
		IncreaseCombo(registry, sourceEntitiy);
	}
	else if(registry.has<CPlayerBallComponent>(sourceEntitiy))
	{
		const CPlayerBallComponent& playerBall = registry.get<CPlayerBallComponent>(sourceEntitiy);
		const entt::entity& playerEntity = playerBall.m_Player;
		if (registry.valid(playerEntity) && registry.has<CPlayerPaddleComponent>(playerEntity))
		{
			IncreaseCombo(registry, playerEntity);
		}
	}


}
void game::SComboSystem::IncreaseCombo(entt::registry& registry, const entt::entity& playerEnttiy)
{
	if (registry.has<CComboComponent>(playerEnttiy))
	{
		CComboComponent& comboComponent = registry.get<CComboComponent>(playerEnttiy);
		comboComponent.m_ComboCounter++;
		comboComponent.m_ComboLifeTime = s_DefaultComboTimer;
		comboComponent.m_UpdatedHUD = false;
	}
	else
	{
		registry.emplace_or_replace<CComboComponent>(playerEnttiy, 1, s_DefaultComboTimer);
	}
}

void game::SComboSystem::OnComboEnded(entt::registry& registry,const int& comboCount ,const entt::entity& sourceEntitiy)
{
	//Will keep the last massage up for 1 second after combo ended 
	m_gameplayHUDSystem.RequestPlayerCombo(comboCount, sourceEntitiy);
	registry.remove<CComboComponent>(sourceEntitiy);
}