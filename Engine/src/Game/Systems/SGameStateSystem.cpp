#include "PCH.h"
#include "SGameStateSystem.h"

#include "game\Components\CPlayerPaddleComponent.h"
#include "game\Components\CBrickComponent.h"

game::SGameStateSystem::SGameStateSystem(engine::MAppManager& appManager, engine::MSoundManager& soundManager)
	:m_AppManager(appManager),
	m_SoundManager(soundManager)
{

}

void game::SGameStateSystem::Update(entt::registry& registry)
{
	//Lose 
	auto playerPaddleView = registry.view<CPlayerPaddleComponent>();

	for (auto& entity : playerPaddleView)
	{
		const CPlayerPaddleComponent& playerPuddle = registry.get<CPlayerPaddleComponent>(entity);
		if (playerPuddle.m_Lifes <= 0)
		{
			if (m_AppManager.GetCurrentAppState(registry) == AppState::AppState_Running)
			{
				m_AppManager.SetAppState(registry, AppState::AppState_GameOver);
				m_SoundManager.PlaySound("Lose");

			}
		}
	}
	//Win 
	auto brickComponentsView = registry.view<CBrickComponent>();
	if (brickComponentsView.empty())
	{
		if (m_AppManager.GetCurrentAppState(registry) == AppState::AppState_Running)
		{
			m_AppManager.SetAppState(registry, AppState::AppState_Win);
			m_SoundManager.PlaySound("Win");
		}
	}
}