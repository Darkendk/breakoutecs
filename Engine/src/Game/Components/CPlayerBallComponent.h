#pragma once
#include <entt\entt.hpp>
#include <SFML\System.hpp>

struct CPlayerBallComponent
{
public:
    entt::entity m_Player;
    bool m_Docked = true;
    //Used to offset the position off the ball relative to the Paddleboard
    sf::Vector2f m_DockingOffsetPosition;
};
