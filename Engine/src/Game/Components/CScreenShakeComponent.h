#pragma once
#include <entt\entt.hpp>
#include <SFML/System.hpp>

struct CScreenShakeComponent
{
public:
    float m_ShakeTimer{};
    sf::Vector2i m_ScreenInitPosition{};
    int m_ShakeCounter = 1;
    int m_ShakeIntencity = 3;
};