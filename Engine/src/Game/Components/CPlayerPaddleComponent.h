#pragma once
#include <entt\entt.hpp>
#include <SFML\Graphics\Color.hpp>

struct CPlayerPaddleComponent
{
public:
    sf::Color m_Color{};
    int m_PlayerIndex{1};
    int m_Score{0};
    int m_Lifes{3};
};
