#pragma once

struct CPlayerComboMessage
{
public:
    int m_ComboScore{0};
    float m_Lifetime{0};
};
