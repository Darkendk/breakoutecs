#pragma once

struct CComboComponent
{
public:
    int m_ComboCounter{1};
    float m_ComboLifeTime{};
    bool m_UpdatedHUD{false};
};