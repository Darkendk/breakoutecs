#include "PCH.h"
#include "Engine/Managers/MSystemsManager.h"
int main()
{
    MSystemsManager instance;
    instance.Init();
    instance.Activate();

    return 0;
}