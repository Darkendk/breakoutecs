#include "PCH.h"
#include "Engine/Systems/SEngineInputSystem.h"

engine::SInputSystem::SInputSystem(MAppManager& appManager)
	:m_AppManager(appManager)
{

}
void engine::SInputSystem::Update(entt::registry& registry)
{
	m_AppManager.ListenToWindowEvents();
	m_AppManager.UpdateAppStates(registry);
}