#pragma once

#include <entt/entt.hpp>
#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>

#include "Engine/Managers/MAppManager.h"

#include "Engine/Systems/System.h"

#include "Engine/Components/CTransformComponent.h"
#include "Engine/Components/CInputComponent.h"

#include "Game/Components/CPlayerPaddleComponent.h"
#include "Game/Components/CPlayerBallComponent.h"

/// \brief Input System, takes care of all player input and controls with the game
namespace engine 
{
	class SInputSystem : System
	{
	public:	
		SInputSystem(MAppManager& appManager);
		void Update(entt::registry& registry);
	private:
		MAppManager& m_AppManager;
	};
}
