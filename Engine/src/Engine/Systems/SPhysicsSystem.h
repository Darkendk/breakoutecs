#pragma once
#include <entt/entt.hpp>
#include <SFML/Graphics.hpp>

#include "Engine/Systems/System.h"

#include "Engine/Managers/MAppManager.h"

#include "Engine/Components/CTransformComponent.h"
#include "Engine/Components/CCollisionComponent.h"
#include "Engine/Components/CColliderComponent.h"

/// \brief PhysicsSystem takes care of all the collisions, movement and gravity in the game
namespace engine
{
	class SPhysicsSystem : System
	{
	public:
		SPhysicsSystem(MAppManager& appManager);
		void Update(entt::registry& registry, float deltaTime);
		void RequestCollision(entt::entity& entiy, CollisionDirection& dircetion);
		void RequestCollision(entt::registry& registry, entt::entity& entity, entt::entity& otherTransform);
		bool CheckForWindowCollisions(entt::registry& registry,entt::entity& entity, const CTransformComponent& transform, int& windowWidth, int& windowHeight);
		bool CheckForObjectCollisions(entt::registry& registry,entt::entity& sourceEntity, const CTransformComponent& sourcetransform);
		void CreateCollisions(entt::registry& registry);
	private:
		MAppManager& m_AppManager;

		//Collison request to create the collision component
		struct CollisionRequest
		{
			//Constractor to be able to use Emplace in the vector
			CollisionRequest(const entt::entity entity, const std::variant<CollisionDirection, entt::entity> collsionInfo)
				:m_Entity(entity), m_CollsionInfo(collsionInfo)
			{
			}
			const entt::entity m_Entity;
			const std::variant<CollisionDirection, entt::entity> m_CollsionInfo;
		};
		std::vector<CollisionRequest> m_CollisionRequests;
	};
}
