#include "PCH.h"
#include "Engine/Systems/SPhysicsSystem.h"
#include "Engine/Components/CShapeComponent.h"


engine::SPhysicsSystem::SPhysicsSystem(MAppManager& appManager)
	:m_AppManager(appManager)
{

}

void engine::SPhysicsSystem::Update(entt::registry& registry, float deltaTime)
{
	const sf::RenderWindow& window = m_AppManager.GetAppWindow();
	int windowWidth = window.getSize().x;
	int windowHeight = window.getSize().y;

	// For every entity with a Transform component and Collision component...
	registry.view<CTransformComponent, CColliderComponent>().each([&](auto entity, CTransformComponent& transform, CColliderComponent& collider)
	{
		// The velocity.x is applied immidiatly per fame there is no friction
		transform.m_Position.x += deltaTime * transform.m_Velocity.x;
		transform.m_Position.y += deltaTime * transform.m_Velocity.y;

		//Only check for dynamic objects
		if (collider.m_ColliderType == CColliderComponent::ColliderType::ColliderType_Dynaimc)
		{
			bool colidedWithObject = CheckForObjectCollisions(registry, entity, transform);
			if (!colidedWithObject)
			{
				CheckForWindowCollisions(registry, entity, transform, windowWidth, windowHeight);
			}
		}
		//Stop any shape from being rendered outsize of the window size
		transform.m_Position.x = std::clamp(transform.m_Position.x, 0.f, window.mapPixelToCoords({ windowWidth ,windowHeight }).x - transform.m_Size.x);
		transform.m_Position.y = std::clamp(transform.m_Position.y, 0.f, window.mapPixelToCoords({ windowWidth ,windowHeight }).y - transform.m_Size.y);
	});

	CreateCollisions(registry);
}

void engine::SPhysicsSystem::RequestCollision(entt::entity& entity, CollisionDirection& direction)
{
	m_CollisionRequests.emplace_back(entity ,direction);
}

void engine::SPhysicsSystem::RequestCollision(entt::registry& registry, entt::entity& entity, entt::entity& otherEntity)
{
	m_CollisionRequests.emplace_back(entity ,otherEntity);
}

bool engine::SPhysicsSystem::CheckForObjectCollisions(entt::registry& registry, entt::entity& sourceEntity, const CTransformComponent& sourcetransform)
{
	bool collisionDetected = false;
	registry.view<CTransformComponent, CColliderComponent>().each([&](auto entity, CTransformComponent& transform, CColliderComponent& collider)
	{
		// Don't check collisions against yourself
		if (entity != sourceEntity)
		{
			//Horizontal Collision
			float shapeXSize = transform.m_Size.x;
			float SourceShapeSizex = sourcetransform.m_Size.x;

			
			bool horizontalCollision = (sourcetransform.m_Position.x >= (transform.m_Position.x - SourceShapeSizex)&&
				sourcetransform.m_Position.x <= (transform.m_Position.x + shapeXSize));

			if (horizontalCollision)
			{
				// Vertical Collision
				float shapeYSize = transform.m_Size.y;
				if (registry.has<CCircleShapeComponent>(entity))
				{
					//Circles hold the Diametar as their size in the transfrom x component 
					shapeYSize = transform.m_Size.x;
				}

				float SourceShapeSizeY = sourcetransform.m_Size.y;
				bool isSoruceCircle = registry.has<CCircleShapeComponent>(sourceEntity);
				{
					//Circles hold the Diametar as their size in the transfrom x component 
					SourceShapeSizeY = sourcetransform.m_Size.x;
				}
				bool verticalCollisionTop = (sourcetransform.m_Position.y >= (transform.m_Position.y - shapeYSize*2));
				bool verticalCollisionBotton = ((sourcetransform.m_Position.y - SourceShapeSizeY) <= transform.m_Position.y);
				if (verticalCollisionTop && verticalCollisionBotton)
				{
					//std::cout << "source X: " << sourcetransform.m_Position.x << " source Y: " << sourcetransform.m_Position.y << std::endl;
					//std::cout << "other  X: " << transform.m_Position.x << " other Y: " << transform.m_Position.y << std::endl;
					//std::cout << "-----------" << std::endl;
					RequestCollision(registry, sourceEntity, entity);
					collisionDetected = true;
				}
			}
		}
	});
	
	return collisionDetected;
}

bool engine::SPhysicsSystem::CheckForWindowCollisions(entt::registry& registry, entt::entity& entity, const CTransformComponent& transform, int& windowWidth, int& windowHeight)
{
	float shapeXSize = transform.m_Size.x;
	float shapeYSize = transform.m_Size.y;
	if (registry.has<CCircleShapeComponent>(entity))
	{
		//Circles hold the Diameter as their size in the transfrom x component 
		shapeYSize = shapeXSize;
	}
	
	bool rightBorderLimit = transform.m_Position.x >= (windowWidth - shapeXSize);
	bool leftBorderLimit = transform.m_Position.x <= s_Zero;
	bool topBorderLimit = transform.m_Position.y <= s_Zero;
	bool bottomBorderLimit = transform.m_Position.y >= (windowHeight - shapeYSize);

	CollisionDirection collisionDirection = CollisionDirection::CollisionDirection_Unknown;

	if (rightBorderLimit)
	{
		collisionDirection = CollisionDirection::CollisionDirection_Right;
	}

	if (leftBorderLimit)
	{
		collisionDirection = CollisionDirection::CollisionDirection_Left;
	}

	if (topBorderLimit)
	{
		collisionDirection = CollisionDirection::CollisionDirection_Top;
	}

	if (bottomBorderLimit)
	{
		collisionDirection = CollisionDirection::CollisionDirection_Bottom;
	}

	if (collisionDirection != CollisionDirection::CollisionDirection_Unknown)
	{
		RequestCollision(entity, collisionDirection);
		return true;
	}
	return false;
}

void engine::SPhysicsSystem::CreateCollisions(entt::registry& registry)
{
	registry.view<CCollisionComponent>().each([&](auto entity, CCollisionComponent& collision)
	{
		registry.remove<CCollisionComponent>(entity);
	});

	for (const auto& collsionRequest : m_CollisionRequests)
	{
		//In case we already have a collision component we replace it otherwise make a new one
		registry.emplace_or_replace<CCollisionComponent>(collsionRequest.m_Entity, collsionRequest.m_CollsionInfo);
	}
	m_CollisionRequests.clear();
}