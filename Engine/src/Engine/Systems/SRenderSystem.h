#pragma once
#include <iostream>
#include <entt/entt.hpp>

#include "Engine/Managers/MAppManager.h"

#include "Engine/Systems/System.h"

#include "Engine/Components/CTransformComponent.h"
#include "Engine/Components/CShapeComponent.h"

/// \brief Render System in charge or drawing all shapes and objects on the screen
namespace engine
{
	class SRenderSystem : System
	{
	public:
		SRenderSystem(MAppManager& appManager);
		void Update(entt::registry& registry);
		void Draw(entt::registry& registry);
	private:
		MAppManager& m_AppManager;
	};
}
