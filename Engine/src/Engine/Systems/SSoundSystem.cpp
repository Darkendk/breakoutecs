#include "PCH.h"
#include "Engine/Systems/SSoundSystem.h"
#include <cmath>
#include <fstream>

static int WriteSoundBuffer(const void* input,
    void* output,
    unsigned long frameCount,
    const PaStreamCallbackTimeInfo* timeInfo,
    PaStreamCallbackFlags statusFlags,
    void* userData);

float gainToDB(float gain)
{
    return 20 * std::log10(gain);
}

float dBToGain(float dB)
{
    return std::pow(10.f, dB / 20.f);
}

engine::SSoundSystem::SSoundSystem(entt::registry& registry)
	:m_stream(nullptr),
	m_Registry(registry)
{
	m_VoiceRequests.reserve(INIT_VOICE_REQUEST_ALLOC);
}

engine::SSoundSystem::~SSoundSystem()
{
    PaError err = Pa_StopStream(m_stream);

    if (err != paNoError)
    {
        printf("PortAudio error: %s\n", Pa_GetErrorText(err));
    }
}

void engine::SSoundSystem::Activate()
{
	//Initilize Sound engine
	Pa_Initialize();
	PaStreamParameters outputParams;
	outputParams.device = Pa_GetDefaultOutputDevice();
	ASSERT(outputParams.device != paNoDevice && "No Device detected");
	outputParams.channelCount = 2;
	outputParams.sampleFormat = paFloat32;
	outputParams.suggestedLatency = Pa_GetDeviceInfo(outputParams.device)->defaultHighOutputLatency;
	outputParams.hostApiSpecificStreamInfo = 0;

	//Currently not support any input no microphones
	Pa_OpenStream(&m_stream, 0, &outputParams, SAMPLE_RATE, 0, paClipOff, WriteSoundBuffer, this);
	ASSERT(m_stream != nullptr && "Could not start stream");
	Pa_StartStream(m_stream);
}
void engine::SSoundSystem::Update(entt::registry& registry)
{
	UpdateVoices(registry);
}

engine::SSoundSystem::ActiveSoundComponentsView engine::SSoundSystem::GetActiveVoices()
{
	return m_Registry.view<CSoundComponent>();
}

void engine::SSoundSystem::MixSound(float* outPtr, unsigned long numFrames, engine::SSoundSystem::ActiveSoundComponentsView soundComponentView)
{
	m_lock.lock();
	std::memset(outPtr, 0, size_t(sizeof(float) * numFrames * 2));
	for (auto& entity : soundComponentView)
	{
		CSoundComponent& soundComponent = soundComponentView.get<CSoundComponent>(entity);
		std::vector<SoundVoice>& voiceList = soundComponent.m_voiceList;
		for (SoundVoice& voice : voiceList)
		{
			ProcessVoice(outPtr, numFrames, voice);
		}
	}

	m_lock.unlock();
}

void engine::SSoundSystem::ProcessVoice(float* outPtr, unsigned long numFrames, SoundVoice& soundVoice)
{
	float* out = outPtr;
	bool stopPlaying = false;
	const AudioFile<double>* wave = soundVoice.m_wave;
	if (!wave)
		return;
	switch (wave->getNumChannels())
	{
		case 1:
		{
			AudioFile<double>::AudioBuffer data = wave->samples;
			unsigned int end = wave->getNumSamplesPerChannel();
			for (unsigned int i = 0; i < numFrames && soundVoice.m_index < end; ++i)
			{
				float next = static_cast<float>(data[0][soundVoice.m_index]);

				// left
				*out++ += next;

				// right
				*out++ += next;

				//Currently no pitch modifiers
				soundVoice.m_index += 1;
			}

			if (unsigned int(soundVoice.m_index) == end)
			{
				stopPlaying = true;
			}

			break;
		}
		case 2:
		{
			AudioFile<double>::AudioBuffer data = wave->samples;
			unsigned int startingFrame = static_cast<unsigned int>(soundVoice.m_index);
			unsigned end = wave->getNumSamplesPerChannel();
			for (unsigned int i = 0; i < numFrames && soundVoice.m_index < end; ++i)
			{
				float left = static_cast<float>(data[0][soundVoice.m_index]);
				float right = static_cast<float>(data[1][soundVoice.m_index]);

				*out++ += left;
				*out++ += right;

				//Currently no pitch modifiers
				soundVoice.m_index += 1;
			}

			if (unsigned int(soundVoice.m_index) == end)
			{
				stopPlaying = true;
			}
			break;
		}
	}

	if (stopPlaying)
	{
		if (!soundVoice.m_isLooping)
		{
			soundVoice.m_isPlaying = false;
		}
		else
		{
			//loop to the start of the wave index
			soundVoice.m_index = 0;
		}
	}
}

void engine::SSoundSystem::AddSoundVoice(entt::registry& registry, const entt::entity& entity, const AudioFile<double>* soundData, const bool looping)
{
	if (registry.has<CSoundComponent>(entity))
	{
		m_VoiceRequests.emplace_back(entity, soundData, looping);
	}
	else
	{
		//Create new Sound Voice Component
		registry.emplace<CSoundComponent>(entity, soundData, looping);
	}
}

void engine::SSoundSystem::UpdateVoices(entt::registry& registry)
{
	engine::SSoundSystem::ActiveSoundComponentsView soundComponentView = registry.view<CSoundComponent>();
	for (auto& voice : soundComponentView)
	{
		CSoundComponent& soundComponent = soundComponentView.get<CSoundComponent>(voice);
		std::vector<SoundVoice>& voiceList = soundComponent.m_voiceList;

		int i = 0;
		for (SoundVoice& voice : voiceList)
		{
			if (!voice.m_isPlaying)
			{
				if (i < voiceList.size())
				{
					std::vector<SoundVoice>::iterator it = voiceList.erase(voiceList.begin() + i);
				}
			}
			i++;
		}
	}

	for (VoiceRequest& voiceRequest : m_VoiceRequests)
	{
		CSoundComponent& soundVoiceComponent = registry.get<CSoundComponent>(voiceRequest.m_entity);
		soundVoiceComponent.m_voiceList.emplace_back(voiceRequest.m_soundData, voiceRequest.m_looping);
	}
	m_VoiceRequests.clear();
}

static int WriteSoundBuffer(const void* inputBuffer, void* outputBuffer,
	unsigned long framesPerBuffer,
	const PaStreamCallbackTimeInfo* timeInfo,
	PaStreamCallbackFlags statusFlags,
	void* userData)
{
	float* out = reinterpret_cast<float*>(outputBuffer);
	engine::SSoundSystem* soundSys = reinterpret_cast<engine::SSoundSystem*>(userData);
	soundSys->MixSound(out, framesPerBuffer, soundSys->GetActiveVoices());
	return 0;
}

