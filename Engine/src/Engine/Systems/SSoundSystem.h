#pragma once
#include <portaudio.h>
#include <entt\entt.hpp>


#include "Engine/Systems/System.h"
#include "Engine/Components/CSoundComponent.h"

#include <mutex>
#define SAMPLE_RATE (48000)
#define INIT_VOICE_REQUEST_ALLOC (30)

namespace engine
{
    class SSoundSystem : System
    {
    public:
        using ActiveSoundComponentsView = entt::basic_view<entt::entity, entt::exclude_t<>, CSoundComponent>;
        SSoundSystem(entt::registry& registry);
        ~SSoundSystem();
        void Activate();
        void Update(entt::registry& registry);
        void ProcessVoice(float* outPtr, unsigned long numFrames, SoundVoice& soundVoice);
        void MixSound(float* outPtr, unsigned long numFrames, engine::SSoundSystem::ActiveSoundComponentsView voicesView);
        ActiveSoundComponentsView GetActiveVoices();
        void AddSoundVoice(entt::registry& registry, const entt::entity& entity, const AudioFile<double>* soundData, const bool looping);
        void UpdateVoices(entt::registry& registry);
    private:
        struct VoiceRequest
        {
            //Constractor to be able to use emplace 
            VoiceRequest(const entt::entity entity, const AudioFile<double>* soundData, const bool looping)
                :m_entity(entity), m_soundData(soundData), m_looping(looping)
            {}

            const entt::entity m_entity;
            const AudioFile<double>* m_soundData;
            const bool m_looping;
        };
        std::vector<VoiceRequest> m_VoiceRequests;
        std::mutex m_lock;
        PaStream* m_stream = nullptr;
        entt::registry& m_Registry;
    };

}


