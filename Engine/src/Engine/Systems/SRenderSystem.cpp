#include "PCH.h"
#include "Engine/Systems/SRenderSystem.h"

engine::SRenderSystem::SRenderSystem(MAppManager& appManager)
    :m_AppManager(appManager)
{

}
void engine::SRenderSystem::Update(entt::registry& registry)
{
    // System only cares about entities with CShapeType and CTransform
    auto rectangleView = registry.view<CRectangleShapeComponent, CTransformComponent>();
    auto circleView = registry.view<CCircleShapeComponent, CTransformComponent>();

    for (auto& entity : rectangleView)
    {
        auto& transform = registry.get<CTransformComponent>(entity);
        auto& shape = registry.get<CRectangleShapeComponent>(entity);

        shape.m_Shape.setPosition(transform.m_Position);
        shape.m_Shape.setSize(transform.m_Size);
        shape.m_Shape.setRotation(transform.m_Rotation);
    }

    for (auto& entity : circleView)
    {
        auto& transform = registry.get<CTransformComponent>(entity);
        auto& shape = registry.get<CCircleShapeComponent>(entity);
        // For Circles size in the transform is used for Diameter and Point count
        shape.m_Shape.setRadius(transform.m_Size.x* s_Half);
        shape.m_Shape.setPointCount(static_cast<size_t>(transform.m_Size.y));
        shape.m_Shape.setPosition(transform.m_Position);
        shape.m_Shape.setRotation(transform.m_Rotation);
    }
}

void engine::SRenderSystem::Draw(entt::registry& registry)
{
    // For every Shape, draw it.
    registry.view<CRectangleShapeComponent>().each([&](auto& shape)
    {
        m_AppManager.DrawOnWindow(shape.m_Shape);
    });

    registry.view<CCircleShapeComponent>().each([&](auto& shape)
    {
        m_AppManager.DrawOnWindow(shape.m_Shape);
    });
}