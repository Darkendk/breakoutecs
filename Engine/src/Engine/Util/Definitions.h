#pragma once

#define ASSERT(cond) \
	if((cond)) {} else { __debugbreak(); }

namespace
{
    inline constexpr float s_Zero = 0;
    inline constexpr float s_Half = 0.5f;
    inline constexpr float s_Double = 2.0f;
    inline constexpr float s_Queter = 0.25f;
}