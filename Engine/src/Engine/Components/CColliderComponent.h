#pragma once

struct CColliderComponent
{
	enum ColliderType
	{
		ColliderType_Dynaimc,
		ColliderType_Static,
		ColliderType_InputControlled
	};
	ColliderType m_ColliderType{ColliderType::ColliderType_Static};
};