#pragma once
#include <SFML/Window.hpp>

struct CInputComponent
{
public:
	enum ControlScheme {
		ControlScheme_Keyboard,
		ControlScheme_Mouse
	};
	std::vector<sf::Keyboard::Key> m_KeyboardControls{};
	std::vector<sf::Mouse::Button> m_MouseButtons{};
	std::vector<sf::Mouse::Movement> m_MouseMovement{};
	ControlScheme m_CurrentControlScheme{ ControlScheme::ControlScheme_Mouse };
};
