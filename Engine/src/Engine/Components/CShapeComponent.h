#pragma once
#include<SFML/Graphics.hpp>

class CRectangleShapeComponent
{
public:
    sf::RectangleShape m_Shape{};
};

class CCircleShapeComponent
{
public:
    sf::CircleShape m_Shape{};
};