#pragma once

enum class AppState {
	AppState_Running,
	AppState_Resetting,
	AppState_Paused,
	AppState_GameOver,
	AppState_Win,
	AppState_Unknown

};

struct CGlobalAppComponent
{
	AppState m_CurrentAppState{ AppState::AppState_Running };
};