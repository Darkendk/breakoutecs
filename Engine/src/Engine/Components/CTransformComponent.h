#pragma once

#include <SFML/System.hpp>

struct CTransformComponent 
{
public:
	sf::Vector2f m_Position{};
	sf::Vector2f m_Velocity{};
	sf::Vector2f m_Size{};
	float m_Rotation{};
};