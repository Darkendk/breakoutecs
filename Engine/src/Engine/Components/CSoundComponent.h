#pragma once
#include "Engine/DataStuctures/AudioFile.h"

#define VOICE_COUNT_INITAL_ALLOC (15)

struct AudioSample
{
	float left;
	float right;
};

struct SoundVoice
{
		SoundVoice(const AudioFile<double>* asset, bool looping) :
		m_wave(asset),
		m_index(0),
		m_volume(1.f),
		m_isPlaying(true),
		m_isLooping(looping)
		{}

		const AudioFile<double>* m_wave;
		unsigned int m_index;
		float m_volume;
		bool m_isPlaying;
		bool m_isLooping;
};
struct CSoundComponent
{
	CSoundComponent(const AudioFile<double>* asset, bool looping) 
	{
		m_voiceList.reserve(VOICE_COUNT_INITAL_ALLOC);
		m_voiceList.emplace_back(asset, looping);
	}
	std::vector<SoundVoice> m_voiceList;
};