#pragma once
#include "Engine/DataStuctures/AudioFile.h"
struct CGlobalEventListComponent
{
    using SoundData = std::vector<AudioFile<double>*>;
    using EventName = std::string;
    std::map<EventName, SoundData> m_EventList;
};