#pragma once

#include "Engine/Components/CTransformComponent.h"
#include <entt/entt.hpp>
#include <variant>

enum class CollisionDirection {
	CollisionDirection_Unknown,
	CollisionDirection_Left,
	CollisionDirection_Right,
	CollisionDirection_Top,
	CollisionDirection_Bottom
};

struct CCollisionComponent
{
	std::variant<CollisionDirection, entt::entity> m_CollisionInfo{CollisionDirection::CollisionDirection_Unknown};
};

