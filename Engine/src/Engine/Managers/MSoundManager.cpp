#include "PCH.h"
#include "Engine/Managers/MSoundManager.h"


engine::MSoundManager::MSoundManager(SSoundSystem& soundSystem)
    :m_SoundSystem(soundSystem)
{
    m_PlaySoundRequests.reserve(INIT_SOUND_REQUESTS_PER_FRAME);
}

engine::MSoundManager::~MSoundManager()
{

}

namespace
{
    bool IsVoicePlaying(SoundVoice& voice) {
        return voice.m_isPlaying;
    }

}
void engine::MSoundManager::Activate(entt::registry& registry)
{
    auto globalSoundEntity = registry.create();
    CGlobalEventListComponent& globalEventList = registry.emplace<CGlobalEventListComponent>(globalSoundEntity);
    //Music
    LoadSoundEvent(globalEventList, "Music", { "Assets/Sound/Music48_24.wav" });

    //SFX
    LoadSoundEvent(globalEventList, "BrickCollision", "Assets/Sound/BrickImpact_01.wav");
    LoadSoundEvent(globalEventList, "BrickCollision", "Assets/Sound/BrickImpact_02.wav");
    LoadSoundEvent(globalEventList, "BrickCollision", "Assets/Sound/BrickImpact_03.wav");
    LoadSoundEvent(globalEventList, "BrickCollision", "Assets/Sound/BrickImpact_04.wav");
    LoadSoundEvent(globalEventList, "BrickCollision", "Assets/Sound/BrickImpact_05.wav");
    LoadSoundEvent(globalEventList, "BrickCollision", "Assets/Sound/BrickImpact_06.wav");
    LoadSoundEvent(globalEventList, "BrickCollision", "Assets/Sound/BrickImpact_07.wav");
    LoadSoundEvent(globalEventList, "BrickCollision", "Assets/Sound/BrickImpact_08.wav");
    LoadSoundEvent(globalEventList, "BrickCollision", "Assets/Sound/BrickImpact_09.wav");

    LoadSoundEvent(globalEventList, "BallRedock", "Assets/Sound/BallDock_01.wav");
    LoadSoundEvent(globalEventList, "BallRedock", "Assets/Sound/BallDock_02.wav");
    LoadSoundEvent(globalEventList, "BallRedock", "Assets/Sound/BallDock_03.wav");
    LoadSoundEvent(globalEventList, "BallRedock", "Assets/Sound/BallDock_04.wav");
    LoadSoundEvent(globalEventList, "BallRedock", "Assets/Sound/BallDock_05.wav");

    LoadSoundEvent(globalEventList, "PaddleImpact", "Assets/Sound/PaddleImpact_01.wav");
    LoadSoundEvent(globalEventList, "PaddleImpact", "Assets/Sound/PaddleImpact_02.wav");
    LoadSoundEvent(globalEventList, "PaddleImpact", "Assets/Sound/PaddleImpact_03.wav");
    LoadSoundEvent(globalEventList, "PaddleImpact", "Assets/Sound/PaddleImpact_04.wav");
    LoadSoundEvent(globalEventList, "PaddleImpact", "Assets/Sound/PaddleImpact_05.wav");
    LoadSoundEvent(globalEventList, "PaddleImpact", "Assets/Sound/PaddleImpact_06.wav");

    LoadSoundEvent(globalEventList, "Win", "Assets/Sound/Win.wav");

    LoadSoundEvent(globalEventList, "Lose", "Assets/Sound/Lose.wav");

    //Play Music
    PlaySound("Music",true);
}

void engine::MSoundManager::Update(entt::registry& registry)
{
    //Add New Sound Voices
    if (!m_PlaySoundRequests.empty())
    {
        const CGlobalEventListComponent* globalEventList = GetGlobalEventList(registry);
        if (!globalEventList)
        {
            ASSERT(true && "Can't load Global Event List")
            return;
        }

        for (auto& playSoundRequest : m_PlaySoundRequests)
        {
            auto it = globalEventList->m_EventList.find(playSoundRequest.m_eventName);

            if (it != globalEventList->m_EventList.end())
            {
                const AudioFile<double>* soundData = GetAudioDataFromEvent(it->second);
                if (playSoundRequest.m_entity == entt::null)
                {
                    const entt::entity entity = GetGlobalSoundEntity(registry);
                    m_SoundSystem.AddSoundVoice(registry, entity, soundData, playSoundRequest.m_looping);
                }
                else
                {
                    m_SoundSystem.AddSoundVoice(registry, playSoundRequest.m_entity, soundData, playSoundRequest.m_looping);
                }
            }
            else
            {
                ASSERT(true && "Can't find event name, Double check if it is loaded");
            }
        }
    }
    m_PlaySoundRequests.clear();
    //m_SoundSystem.UpdateVoice(registry);
}

void engine::MSoundManager::PlaySound(const char* eventName, bool looping /* = false*/, entt::entity entity /* eentt::null*/)
{
    m_PlaySoundRequests.emplace_back(eventName, looping, entity);
}

const CGlobalEventListComponent* engine::MSoundManager::GetGlobalEventList(entt::registry& registry)
{
    auto globalSoundEventView = registry.view<CGlobalEventListComponent>();
    for (auto& entity : globalSoundEventView)
    {
        return &registry.get<CGlobalEventListComponent>(entity);
    }
    return nullptr;
}

const entt::entity engine::MSoundManager::GetGlobalSoundEntity(entt::registry& registry)
{
    auto globalSoundEventView = registry.view<CGlobalEventListComponent>();
    for (auto& entity : globalSoundEventView)
    {
        return entity;
    }
    return entt::null;
}

AudioFile<double>* engine::MSoundManager::GetAudioDataFromEvent(const std::vector<AudioFile<double>*>& eventAudioData)
{
    if (!eventAudioData.empty())
    {
        //Suppot Sound Variation and random containers
        if (eventAudioData.size() > 1)
        {
            int variationNumber = std::rand() % eventAudioData.size();
            return eventAudioData[variationNumber];
        }
        else
        {
            return eventAudioData[0];
        }
    }
    else
    {
        ASSERT(!eventAudioData.empty() && "Event Sound Data is empty please double check if the PlaySoundRequest is correct");
    }
    return nullptr;
}

void engine::MSoundManager::LoadSoundEvent(CGlobalEventListComponent& globalEventList, const std::string& eventName, const char* filePath)
{
    auto it = globalEventList.m_EventList.find(eventName);

    if (it != globalEventList.m_EventList.end())
    {
        it->second.emplace_back(new AudioFile<double>(filePath));
    }
    else
    {
        CGlobalEventListComponent::SoundData soundData;
        soundData.emplace_back(new AudioFile<double>(filePath));
        globalEventList.m_EventList.emplace(eventName, soundData);    
    }
}