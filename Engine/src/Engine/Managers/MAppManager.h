#pragma once
#include <entt\entt.hpp>
#include <SFML\Graphics.hpp>

#include "Engine\Components\CGlobalAppComponent.h"

namespace engine
{
    class MAppManager
    {
    public:
        MAppManager();
        ~MAppManager();

        void Activate(entt::registry& registry);
        void ListenToWindowEvents();
        void UpdateAppStates(entt::registry& registry);

        void SetAppWindow(sf::RenderWindow* window);
        const sf::RenderWindow& GetAppWindow() { return *m_AppWindow; }
        sf::RenderWindow& GetNoneConstAppWindow() { return *m_AppWindow; }

        void ClearWindow();
        void DisplayWindow();
        void DrawOnWindow(const sf::Drawable& drawable);
        void SetAppState(entt::registry& registry, AppState state);
        AppState GetCurrentAppState(entt::registry& registry);
    private:
        sf::VideoMode GetBestPossibleResolution();
        sf::RenderWindow* m_AppWindow;
        entt::entity m_GlobalAppEntity;
    };
}
