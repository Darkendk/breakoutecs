#include "PCH.h"
#include "Engine/Managers/MAppManager.h"
namespace
{
    constexpr const char* s_WindowTitle = "Game";
    constexpr int s_W = 1920;
    constexpr int s_H = 1080;
}

engine::MAppManager::MAppManager()
    :m_AppWindow(nullptr)
{

}

engine::MAppManager::~MAppManager()
{
    if (m_AppWindow)
        delete m_AppWindow;
}

void engine::MAppManager::Activate(entt::registry& registry)
{
    auto appEntity = registry.create();
    registry.emplace<CGlobalAppComponent>(appEntity);
    //Create a window
    //sf::RenderWindow* window = new sf::RenderWindow(GetBestPossibleResolution(), s_WindowTitle, sf::Style::Close);
    sf::RenderWindow* window = new sf::RenderWindow(sf::VideoMode(s_W, s_H), s_WindowTitle, sf::Style::Close);

    //Set window Parameters
    window->setMouseCursorVisible(false);
    window->setMouseCursorGrabbed(true);
    window->setFramerateLimit(60);
    window->setKeyRepeatEnabled(false);
    window->setVerticalSyncEnabled(true);
    
    //Store Window
    SetAppWindow(window);
}

void engine::MAppManager::ListenToWindowEvents()
{
    sf::Event event{};
    while (m_AppWindow->pollEvent(event))
    {
        if (event.type == sf::Event::Closed)
            m_AppWindow->close();

        if (event.type == sf::Event::Resized)
        {
            unsigned int newWidth = event.size.width;
            unsigned int newHeight = event.size.height;
            m_AppWindow->setSize({ newWidth, newHeight });
        }
    }

    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape))
        m_AppWindow->close();
}

void engine::MAppManager::UpdateAppStates(entt::registry& registry)
{
    const AppState& appState = GetCurrentAppState(registry);
    //if game was resetting previous frame we got back to Running
    if (appState == AppState::AppState_Resetting)
    {
        SetAppState(registry, AppState::AppState_Running);
    }

    //GameOverState wait for input
    if (appState == AppState::AppState_GameOver || 
        appState == AppState::AppState_Win )
    {
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Enter))
            SetAppState(registry, AppState::AppState_Resetting);

        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space))
            SetAppState(registry, AppState::AppState_Resetting);

        if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
            SetAppState(registry, AppState::AppState_Resetting);
    }
}

void engine::MAppManager::SetAppWindow(sf::RenderWindow* window)
{
    //In case there is an already existing Window
    if (m_AppWindow != nullptr)
    {
        //Delete Window to avoid leaks
        delete m_AppWindow;
    }

    m_AppWindow = window; 
}

void engine::MAppManager::ClearWindow()
{
    m_AppWindow->clear(sf::Color(0, 0, 0));
}

void engine::MAppManager::DisplayWindow()
{
    m_AppWindow->display();
}

void engine::MAppManager::DrawOnWindow(const sf::Drawable& drawable)
{
    m_AppWindow->draw(drawable);
}

sf::VideoMode engine::MAppManager::GetBestPossibleResolution()
{
    const std::vector<sf::VideoMode>& videoModes = sf::VideoMode::getFullscreenModes();
    if (!videoModes.empty())
    {
        return videoModes[static_cast<unsigned __int64>(s_Zero)];
    }
    else
    {
        return sf::VideoMode::getDesktopMode();
    }
}

void engine::MAppManager::SetAppState(entt::registry& registry,AppState state)
{
    auto globalAppComponentView = registry.view<CGlobalAppComponent>();

    for (auto& entity : globalAppComponentView)
    {
        CGlobalAppComponent& globalAppComponent = globalAppComponentView.get<CGlobalAppComponent>(entity);
        globalAppComponent.m_CurrentAppState = state;
    }
}

AppState engine::MAppManager::GetCurrentAppState(entt::registry& registry)
{
    auto globalAppComponentView = registry.view<CGlobalAppComponent>();

    for (auto& entity : globalAppComponentView)
    {
        CGlobalAppComponent& globalAppComponent = globalAppComponentView.get<CGlobalAppComponent>(entity);
        return globalAppComponent.m_CurrentAppState;
    }

    ASSERT(true && "Can't set App state please make sure AppManager Was activated properly");
    return AppState::AppState_Unknown;
}
