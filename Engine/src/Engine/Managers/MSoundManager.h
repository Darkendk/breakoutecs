#pragma once
#include <entt/entt.hpp>

#include "Engine/Components/CGlobalEventListComponent.h"
#include "Engine/Components/CSoundComponent.h"
#include "Engine/Components/CTransformComponent.h"

#include "Engine/Systems/SSoundSystem.h"

#define  INIT_SOUND_REQUESTS_PER_FRAME (30)
namespace engine
{
    class MSoundManager
    {
    public:
        MSoundManager(SSoundSystem& soundSystem);
        ~MSoundManager();
        void Activate(entt::registry& registry);
        void Update(entt::registry& registry);
        void PlaySound(const char* eventName, bool looping = false, entt::entity entity = entt::null);
        const CGlobalEventListComponent* GetGlobalEventList(entt::registry& registry);
        const entt::entity GetGlobalSoundEntity(entt::registry& registry);
        AudioFile<double>* GetAudioDataFromEvent(const std::vector<AudioFile<double>*>& eventAudioData);
        void LoadSoundEvent(CGlobalEventListComponent& globalEventList, const std::string& eventName, const char* filePath);
    private:
        struct PlaySoundRequest
        {
            //contructor to be able to emplace back elements in the vector
            PlaySoundRequest(const char* eventName, bool looping, entt::entity entity)
                :m_eventName(eventName), m_looping(looping), m_entity(entity)
            {}
            const char* m_eventName;
            bool m_looping;
            entt::entity m_entity;
        };
        std::vector<PlaySoundRequest> m_PlaySoundRequests;
        SSoundSystem& m_SoundSystem;
    };
}