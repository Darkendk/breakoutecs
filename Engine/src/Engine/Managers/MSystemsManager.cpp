#include "PCH.h"
#include "Engine/Managers/MSystemsManager.h"
#include "Engine/Systems/System.h"

MSystemsManager::MSystemsManager()
    :m_AppManager(nullptr),
    m_RenderSystem(nullptr),
    m_EngineInputSystem(nullptr),
    m_PhysicsSystem(nullptr),
    m_PlayerSystem(nullptr),
    m_PlayerBallSystem(nullptr),
    m_GameInputSystem(nullptr),
    m_BrickSystem(nullptr),
    m_ScreenShakeSystem(nullptr),
    m_GameplayHUDSystem(nullptr),
    m_GameStateSystem(nullptr),
    m_ComboSystem(nullptr),
    m_SoundSystem(nullptr)
{

}
MSystemsManager::~MSystemsManager()
{
    //Delete assigned memebers 
    //TODO Automate this 
    if (m_AppManager)
        delete m_AppManager;

    if (m_SoundManager)
        delete m_SoundManager;

    if (m_RenderSystem)
        delete m_RenderSystem;

    if (m_EngineInputSystem)
        delete m_EngineInputSystem;

    if (m_PhysicsSystem)
        delete m_PhysicsSystem;

    if (m_PlayerSystem)
        delete m_PlayerSystem;

    if (m_PlayerBallSystem)
        delete m_PlayerBallSystem;

    if (m_GameInputSystem)
        delete m_GameInputSystem;

    if (m_BrickSystem)
        delete m_BrickSystem;

    if (m_ScreenShakeSystem)
        delete m_ScreenShakeSystem;

    if (m_GameplayHUDSystem)
        delete m_GameplayHUDSystem;

    if (m_GameStateSystem)
        delete m_GameStateSystem;

    if (m_ComboSystem)
        delete m_ComboSystem;

    if (m_SoundSystem)
        delete m_SoundSystem;

}
void MSystemsManager::Init()
{
    //AppSetup
    m_AppManager = new engine::MAppManager();

    //Engine Systems 
    m_EngineInputSystem = new engine::SInputSystem(*m_AppManager);
    m_PhysicsSystem = new engine::SPhysicsSystem(*m_AppManager);
    m_RenderSystem = new engine::SRenderSystem(*m_AppManager);
    //Sound System runs in it's own thread so needs seperate access to the registry 
    m_SoundSystem = new engine::SSoundSystem(m_Registry);

    //Engine Managers
    m_SoundManager = new engine::MSoundManager(*m_SoundSystem);

    //Game Systesms
    m_PlayerSystem = new game::SPlayerSystem(*m_AppManager);
    m_PlayerBallSystem = new game::SPlayerBallSystem(*m_AppManager,*m_SoundManager);
    m_GameInputSystem = new game::SInputSystem(*m_AppManager, *m_PlayerBallSystem);
    m_GameplayHUDSystem = new game::SGameplayHUDSystem(*m_AppManager);
    m_ComboSystem = new game::SComboSystem(*m_AppManager, *m_GameplayHUDSystem);
    m_BrickSystem = new game::SBrickSystem(*m_AppManager, *m_PlayerBallSystem, *m_ComboSystem, *m_SoundManager);
    m_ScreenShakeSystem = new game::SScreenShakeSystem(*m_AppManager,*m_GameplayHUDSystem);
    m_GameStateSystem = new game::SGameStateSystem(*m_AppManager,*m_SoundManager);
}

void MSystemsManager::Activate()
{
    //Activate All Systems 
    m_AppManager->Activate(m_Registry);
    m_SoundManager->Activate(m_Registry);
    m_SoundSystem->Activate();

    m_PlayerSystem->Activate(m_Registry);
    m_PlayerBallSystem->Activate(m_Registry);
    m_BrickSystem->Activate(m_Registry);
    m_GameplayHUDSystem->Activate(m_Registry);
    Update();
}

void MSystemsManager::Update()
{
    sf::Clock Clock;
    const sf::RenderWindow& window = m_AppManager->GetAppWindow();

    while (window.isOpen())
    {
        // DeltaTime is referring to the spacing between two frames
        sf::Time DeltaTime;
        DeltaTime = Clock.restart();

        // The window is cleared and updated each frame
        m_AppManager->ClearWindow();
        

        // The systems will then be updated.
        //Input and Feedback 
        m_EngineInputSystem->Update(m_Registry);
        m_GameplayHUDSystem->Update(m_Registry, DeltaTime.asSeconds());
        m_GameStateSystem->Update(m_Registry);
        m_SoundSystem->Update(m_Registry);
        m_SoundManager->Update(m_Registry);

        //If game is not a paused state
        if (m_AppManager->GetCurrentAppState(m_Registry) <= AppState::AppState_Resetting)
        {
            //Core Engine systms
            m_PhysicsSystem->Update(m_Registry, DeltaTime.asSeconds());
            m_RenderSystem->Update(m_Registry);
            
            // Update Gamepaly Systmes
            m_PlayerSystem->Update(m_Registry);
            m_PlayerBallSystem->Update(m_Registry);
            m_ComboSystem->Update(m_Registry, DeltaTime.asSeconds());
            m_GameInputSystem->Update(m_Registry);
            m_BrickSystem->Update(m_Registry);
            m_ScreenShakeSystem->Update(m_Registry, DeltaTime.asSeconds());
        }

        // Render result after all engine and game calculation
        m_RenderSystem->Draw(m_Registry);

        //Display the widnow
        m_AppManager->DisplayWindow();
    }
}



