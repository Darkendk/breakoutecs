#pragma once
#include <entt\entt.hpp>

#include "Engine\Managers\MAppManager.h"
#include "Engine\Managers\MSoundManager.h"

#include "Engine\Systems\SRenderSystem.h"
#include "Engine\Systems\SEngineInputSystem.h"
#include "Engine\Systems\SPhysicsSystem.h"
#include "Engine\Systems\SSoundSystem.h"


#include "Game\Systems\SPlayerSystem.h"
#include "Game\Systems\SPlayerBallSystem.h"
#include "Game\Systems\SGameInputSystem.h"
#include "Game\Systems\SBrickSystem.h"
#include "Game\Systems\SScreenShakeSystem.h"
#include "Game\Systems\SGameplayHUDSystem.h"
#include "Game\Systems\SGameStateSystem.h"
#include "Game\Systems\SComboSystem.h"

class MSystemsManager
{
public:
	MSystemsManager();
	~MSystemsManager();

	/// \Creates all the Systems and stores them 
	void Init();
	/// \brief Activates the Systems manager and create the current application window and player
	void Activate();
	/// \brief Main update loop, used to update all systems 
	/// \param window the current window
	void Update();

private:

	struct RegisteredSystem 
	{
		System* m_System;
		bool	m_UpdatedOnPause;
	};
	std::vector<RegisteredSystem> m_Systems;
	// Main ECS Registry
	entt::registry m_Registry;

	// Egnine Systems
	engine::SRenderSystem* m_RenderSystem;
	engine::SInputSystem* m_EngineInputSystem;
	engine::SPhysicsSystem* m_PhysicsSystem;
	engine::SSoundSystem* m_SoundSystem;

	// Game Systems
	game::SPlayerSystem* m_PlayerSystem;
	game::SPlayerBallSystem* m_PlayerBallSystem;
	game::SInputSystem* m_GameInputSystem;
	game::SBrickSystem* m_BrickSystem;
	game::SScreenShakeSystem* m_ScreenShakeSystem;
	game::SGameplayHUDSystem* m_GameplayHUDSystem;
	game::SGameStateSystem* m_GameStateSystem;
	game::SComboSystem* m_ComboSystem;

	//Engine Managers 
	engine::MAppManager* m_AppManager;
	engine::MSoundManager* m_SoundManager;

	//Game Managers 
};